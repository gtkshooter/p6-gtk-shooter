use v6.c;

# Find the original implementation here:
# https://gitlab.gnome.org/GNOME/clutter/blob/master/examples/canvas.c

use Cairo;

use GTK::Compat::Types;
use Clutter::Raw::Types;

use Clutter::Actor;
use Clutter::ScrollActor;
use Clutter::BindConstraint;
use Clutter::Cairo;
use Clutter::Canvas;
use Clutter::Color;
use Clutter::Stage;
use Clutter::Threads;

use Clutter::Main;

constant STARCOUNT = 1000;
constant CHUNKSIZE = STARCOUNT div 4;

my $idle_resize_id;

sub make-star-sheet-maker($chunk) {
    -> $c, $cr, $w, $h, $ud, $r --> gboolean {
        my $ctx = Cairo::Context.new($cr);

        $ctx.save;
        $ctx.operator = CAIRO_OPERATOR_CLEAR;
        $ctx.paint;
        $ctx.restore;

        $ctx.line_cap = Cairo::LINE_CAP_ROUND;
        $ctx.rgb(my $val = 1 - $chunk * 0.2, $val, $val);

        my $screen_h = $h div 2;

        for ^CHUNKSIZE {
            my $star_x = $w.rand.Int;
            my $star_y = $screen_h.rand.Int;
            $ctx.move_to($star_x, $star_y);
            $ctx.line_to(0, 0, :relative);
            $ctx.move_to($star_x, $star_y + $screen_h);
            $ctx.line_to(0, 0, :relative);
        }
        $ctx.stroke;

        $r.r = 1;
    }
}

sub MAIN {
  exit(1) unless Clutter::Main.init == CLUTTER_INIT_SUCCESS;

  my $stage = Clutter::Stage.new;
  $stage.setup(
    title          => 'shmup game',
    size           => (800, 600)
  );
  
  my $color = Clutter::Color.new-from-color($CLUTTER_COLOR_Black);
  $color.alpha = 255;
  $stage.background-color = $color;
  $stage.show-actor;

      sub do-the-scroll($actor, $duration) {
          my ($p1, $p2) = Clutter::Point.new xx 2;
          $p1.x = 0e0;
          $p1.y = 600e0;
          $actor.scroll-to-point($p1);
          $actor.save-easing-state;
          $actor.easing-mode = ClutterAnimationMode::CLUTTER_LINEAR;
          $actor.easing-duration = $duration;
          say "building a scroll with $duration";
          $p2.x = 0e0;
          $p2.y = 0e0;
          $actor.scroll-to-point($p2);
          $actor.restore-easing-state;
      }

  my @starfields = do
  for ^3 {
      my $canvas = Clutter::Canvas.new;
      $canvas.set-size(800, 1200);

      my $innerActor = Clutter::Actor.new.setup(
          position => (0,0),
          size => (800, 1200),
      );
      $innerActor.content = $canvas;

      my $actor = Clutter::ScrollActor.new.setup(
          position    => (0, 0),
          scroll-mode => CLUTTER_SCROLL_VERTICALLY,
          constraints => [
              #Clutter::AlignConstraint.new($stage, CLUTTER_ALIGN_X_AXIS, 0),
              Clutter::BindConstraint.new($stage, CLUTTER_BIND_ALL, 0)
          ],
      );
      $actor.add-child($innerActor);

      $stage.add-child($actor);

      my $starfield-maker = make-star-sheet-maker($_);

      $canvas.draw.tap(-> *@a { $starfield-maker(|@a) });

      $canvas.invalidate;

      my $duration = 5000 * 1.8 ** $_;

      do-the-scroll($actor, $duration);

      sub setup-scroll($actor, $duration) {
          $actor.transition-stopped.tap(-> *@a [$actor, *@] { dd $actor; do-the-scroll($actor, $duration) });
      }($actor, $duration);

      {:$canvas, :$actor, :$innerActor}
  }

  #$actor.allocation-changed.tap(-> *@a { on_actor_resize(|@a) });
  $stage.destroy.tap({ Clutter::Main.quit });
  #$canvas.draw.tap(-> *@a { draw_clock(|@a) });

  Clutter::Main.run;
}

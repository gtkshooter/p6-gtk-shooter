sub frame($_, $t is copy, $dt) {
    .push_group;
    .save;
    my num $z = 0e0;
    .rgb($z, $z, $z, $z);
    .operator = CAIRO_OPERATOR_CLEAR;
    .paint;
    .restore;

    .save;
    .scale(2, 2);

    .rgb(1e0, 1e0, 1e0);
    .translate(100, 130);

    .move_to(-30, 20);
    .line_to(60, 0, :relative);
    .line_to(-2, 7, :relative);
    .line_to(60, -15, :relative);
    .line_to(10, -20, :relative);
    .line_to(-60, -20, :relative);

#    .line_to(-76, 0, :relative);
    .line_to(-9, -10, :relative);
    .line_to(-14, 5, :relative);
    .line_to(-30, 0, :relative);
    .line_to(-14, -5, :relative);
    .line_to(-9, 10, :relative);

    .line_to(-60, 20, :relative);
    .line_to(10, 20, :relative);
    .line_to(60, 15, :relative);
    .line_to(-2, -7, :relative);

    .close_path;

    .rgb(0.5e0, 0.5e0, 0.5e0);
    .fill() :preserve;

    my $outlinepath = .copy_path;

    .rgb(1e0, 1e0, 1e0);

    .new_path();

    for ^2 -> $i {
        .move_to(-15 + $i * 28, 20);
        .line_to(-5, 10, :relative);
        .line_to(12, 0, :relative);
        .line_to(-5, -10, :relative);
    }

    .stroke;

    for ^2 -> $i {
        .save;
        .scale(-1, 1) if $i;
        .operator = CAIRO_OPERATOR_ATOP;
        .rgb(1e0, 0e0, 0e0);
        .move_to(50, 30);
        .line_to(20, 0, :relative);
        .line_to(10, -60, :relative);
        .line_to(-30, 0, :relative);
        .close_path;
        .fill;
        .restore;
    }

    .append_path($outlinepath);
    .rgb(1e0, 1e0, 1e0);
    .stroke();


    .move_to(-10, 0);
    .line_to(10, 0);
    .line_to(20, -20);
    .line_to(10, -100);
    .line_to(-10, -100);
    .line_to(-20, -20);
    .line_to(-10, 0);

    .rgb(0.5e0, 0.5e0, 0.5e0);
    .fill :preserve;

    .rgb(1e0, 1e0, 1e0);
    .stroke;
    

    .restore;

    my $ship-image = .pop_group;

    .rgb(0e0, 0e0, 0e0);
    .paint;
    .&grid($t);

    .pattern($ship-image);
    .paint;
}

sub grid($_, $t) {
  .save;

  .operator = CAIRO_OPERATOR_ATOP;
  my num $one = 1e0;
  my num $quarter = 0.125e0;
  my num $half = 0.25e0;

  my num $red = $quarter;
  my num $green = $one;
  my num $blue = $one;

  for 0..60 -> $y {
    .rgb($red, $green, $blue, $quarter) if $y !%% 5;
    .rgb($red, $green, $blue, $half) if $y %% 5;
    .move_to(0, $y * 10);
    .line_to(400, 0, :relative);
    .stroke;
    .stroke if $y %% 5;
  }

  .restore;
  .save;

  for 0..40 -> $x {
    .rgb($red, $green, $blue, $quarter) if $x !%% 5;
    .rgb($red, $green, $blue, $half) if $x %% 5;
    .move_to($x * 10, 0);
    .line_to(0, 600, :relative);
    .stroke;
    .stroke if $x %% 5;
  }
  .restore;
}

&frame;

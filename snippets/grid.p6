sub frame($_, $t is copy, $dt) {
    .save;
    .rgb(1, 2, 3);
    .operator = CAIRO_OPERATOR_CLEAR;
    .paint;
    .restore;

    .&grid($t * 0.1e0);

    .rgb(255, 255, 255);
    .translate(200, 300);
    .move_to(-20, 0);
    .line_to(20, 0);
    .stroke;
}

sub grid($_, $t) {
  .save;

  .operator = CAIRO_OPERATOR_ADD;
  my num $one = 1e0;
  my num $quarter = 0.25e0;
  my num $half = 0.5e0;

  .translate($t.sin * 2, $t.cos * 2);

  for 0..60 -> $y {
    .rgb($one, $one, $one, $quarter) if $y !%% 5;
    .rgb($one, $one, $one, $half) if $y %% 5;
    .move_to(0, $y * 10);
    .line_to(400, 0, :relative);
    .stroke;
    .stroke if $y %% 5;
  }

  .restore;
  .save;
.translate($t.sin * 5, $t.cos * 5);
  for 0..40 -> $x {
    .rgb($one, $one, $one, $quarter) if $x !%% 5;
    .rgb($one, $one, $one, $half) if $x %% 5;
    .move_to($x * 10, 0);
    .line_to(0, 600, :relative);
    .stroke;
    .stroke if $x %% 5;
  }
  .restore;
}

&frame;

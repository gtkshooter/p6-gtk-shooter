use v6.c;

use Pango::FontFamily;

use Cairo;

use GTK::Compat::Types;
use GTK::Raw::Types;

use GTK::Application;
use GTK::Box;
use GTK::Compat::Timeout;
use GTK::Dialog::FileChooser;
use GTK::Dialog::FontChooser;
use GTK::DrawingArea;
use GTK::Label;
use GTK::Menu;
use GTK::MenuBar;
use GTK::MenuItem;
use GTK::ScrolledWindow;
use GTK::ToggleButton;
use GTK::TextBuffer;

use TEPL::Main;
use TEPL::View;

use NativeCall;

sub g-textbuffer-signal-connect(
    GtkTextBuffer $thing,
    Str $name,
    &handler (GObject $target, GParamSpec $pspec, Pointer $userdata-out),
    Pointer $userdata-in)
    is symbol('g_signal_connect_object')
    is native('gobject-2.0')
    { * }

my ($app, $font-d, $file-od, $file-sd, $codeview, $current-file);
my ($compiled, $saved, $last_working_code_text);

sub open-file {
  my $rc = $file-od.run;
  $file-od.current-folder = $*CWD;
  $codeview.text = ($current-file = $file-od.filename).IO.slurp 
    if $rc == GTK_RESPONSE_OK;
  $file-od.hide;
}

sub save-file {
  my $rc = $file-sd.run;
  $file-sd.current-folder = $*CWD;
  ($current-file = $file-sd.filename).IO.spurt($codeview.text) 
    if $rc == GTK_RESPONSE_OK;
  $saved = True;
  $file-sd.hide;
}

sub set-font {
  my $rc = $font-d.run;
  $codeview.override_font( $font-d.font_desc ) if $rc == GTK_RESPONSE_OK;
  $font-d.hide;
}

class CairoContextWrapper {
    has $.context handles
        <save restore
        push_group pop_group
        operator rgb pattern line_width
        scale translate
        paint stroke fill
        close_path new_path copy_path append_path> is rw;

    method line_to($x, $y, Bool :$relative, :$LINENO is copy) {
        #note "line_to $x, $y $(so $relative)";
        $!context.line_to($x, $y, :$relative);

        my $frame = Backtrace.new.list.skip(1).head;
        $LINENO //= $frame.line;
        #$LINENO //= callframe(0).line;

        if $LINENO == $*LINENO {
            note "$frame.file(): ", $LINENO;
            $!context.move_to(-2, -2,  :relative);
            $!context.line_to(4, 0,  :relative);
            $!context.line_to(0, 4,  :relative);
            $!context.line_to(-4, 0, :relative);
            $!context.line_to(0, -4, :relative);
            $!context.move_to(2, 2, :relative);

            $!context.line_to(0, 0, :relative);
        }
        
        CATCH { 
            .say
        }
    }

    method move_to($x, $y, Bool :$relative, :$LINENO is copy) {
        #note "move_to $x, $y $(so $relative)";
        $!context.move_to($x, $y, :$relative);

        my $frame = Backtrace.new.list.skip(1).head;
        $LINENO //= $frame.line;

        if $LINENO == $*LINENO {
            note "$frame.file(): ", $LINENO;
            $!context.move_to(-2, -2,  :relative);
            $!context.line_to(4, 0,  :relative);
            $!context.line_to(0, 4,  :relative);
            $!context.line_to(-4, 0, :relative);
            $!context.line_to(0, -4, :relative);
            $!context.move_to(2, 2, :relative);
        }
        CATCH { 
            .say
        }
    }
}

sub MAIN($filename? is copy) {
    $app = GTK::Application.new( title => 'org.genex.cairo-livecoding' );
    
    $app.activate.tap({
      my $vbox        = GTK::Box.new-vbox(5);
      my $hbox        = GTK::Box.new-hbox(5);
      my $ebox        = GTK::Box.new-vbox;
      my $da          = GTK::DrawingArea.new;
      my $statuslabel = GTK::Label.new('Ready When You Are!');
      my $sw          = GTK::ScrolledWindow.new;
      my $fps_toggle  = GTK::ToggleButton.new-with-label('Slow Mode');

      my $menu = GTK::MenuBar.new(
        GTK::MenuItem.new('_File', :mnemonic
          :submenu(
            GTK::Menu.new(
              GTK::MenuItem.new('_Open', :mnemonic, :clicked(&open-file)),
              GTK::MenuItem.new('_Save', :mnemonic, :clicked(&save-file)),
              GTK::MenuItem.new('_Quit', :mnemonic, clicked => -> { $app.exit })
            )
          ),
        ),
        GTK::MenuItem.new('_Display', :mnemonic,
           :submenu(
             GTK::Menu.new(
               GTK::MenuItem.new('Set _Font', :mnemonic, :clicked(&set-font))
             )
           )
        ),
        # Takes a double click, because... EXPEXTING SUBMENU!
        GTK::MenuItem.new('Help')
      );
      
      $font-d = GTK::Dialog::FontChooser.new('Select a font', $app.window);
      $font-d.set_filter_func(-> $fm, $fc, $ud --> gboolean {
        my $family = Pango::FontFamily.new($fm);
        $family.is-monospace;
      });
      
      $file-od = GTK::Dialog::FileChooser.new(
        'Open File', 
        $app.window,
        GTK_FILE_CHOOSER_ACTION_OPEN
      );
      $file-sd = GTK::Dialog::FileChooser.new(
        'Save File', 
        $app.window,
        GTK_FILE_CHOOSER_ACTION_SAVE
      );
      
      $codeview = TEPL::View.new;
      $codeview.insert-spaces-instead-of-tabs = True;
      ($codeview.tab-width, $codeview.indent-width) = 4 xx 2;
      ($codeview.monospace, $codeview.expand) = True xx 2;
      
      $sw.add($codeview);
      $vbox.margins = 10;
      $ebox.pack-start($_, True) for $menu, $sw, $fps_toggle;
      $hbox.pack-start($_, True) for $ebox, $da;
      $vbox.pack-start($_, True) for $hbox, $statuslabel;
      
      .set_size_request(400, 550) for $sw, $da;
      $app.window.set_size_request(800, 600);
      $app.window.add($vbox);
      $app.window.show_all;
     
      my &frame_handler = -> *@ { };
      my $animation_start_time;
      my $animation_last_t;
      my $animation_last_dt;

      my Supplier::Preserving $cursor-position-supplier .= new;
      g-textbuffer-signal-connect($codeview.buffer."GTK::Raw::Types::GtkTextBuffer"(), "notify::cursor-position",
        -> $obj, $pspec, $udata { $cursor-position-supplier.emit($codeview.buffer.cursor-position) }, Pointer);

      my $cursor-position-supply = $cursor-position-supplier.Supply;

      my $cursor-lineno = -1;

      $cursor-position-supply.stable(0.05).map(-> $curpos {
          $codeview.buffer.get-iter-at-offset($curpos).line();
      } ).stable(0.05).tap({ say "lnie number is now $_"; $cursor-lineno = $_ });

      $codeview.buffer.changed.tap({ $saved = False; $compiled = False });

      $codeview.buffer.changed.stable(1).start(
          -> *@a {
              CATCH { default { .message.say } }

              my $code = @a[0][0].text;
              my &frame_callable;
              $statuslabel.text = "evaling the code now ...";
              try {
                  use MONKEY-SEE-NO-EVAL;

                  &frame_callable = EVAL $code;

                  CATCH {
                      say "error: $_";
                      $statuslabel.text = "Eval failed: $_"
                  }
              }
              if defined &frame_callable {
                  $statuslabel.text = "Evaluation finished.";
              }
              $last_working_code_text = $code;
              $compiled = True;
              &frame_callable
          }).migrate.act(-> &frame_callable {
              &frame_handler = &frame_callable;
          });
      
      my $frame_number;
      my $fast_mode = True;
      
      $fps_toggle.toggled.act(-> *@a { $fast_mode = not @a[0].active; });

      my CairoContextWrapper $cairo-wrapper .= new;

      GTK::Compat::Timeout.simple_timeout(1000 / 25).act(
          -> @ ($t, $dt) {
              if ++$frame_number %% 4 or $fast_mode {
                  $animation_last_t  = $t;
                  $animation_last_dt = $dt;
                  $da.queue_draw;
              }
      
              CATCH {
                  say $_;
              }
          });

      $da.draw.tap(-> *@a {
          # Set return value!
          @a[* - 1].r = 1;
          try {
              $cairo-wrapper.context = Cairo::Context.new(@a[1]);
              my $*LINENO = $cursor-lineno;
              frame_handler($cairo-wrapper, $animation_last_t, $animation_last_dt);
              # Autosave.
                if $current-file && $compiled && $saved.not {
                  "{ $current-file }.working".IO.spurt($last_working_code_text);
                  $saved = True;
                }
              CATCH {
                  default {
                      say "replacing the frame handler with something empty";
                      &frame_handler = -> *@ { }
                      $statuslabel.text = "SORRY! $_";
                      say "Exception in frame handler:";
                      say $_;
                  }
              }
          }
      });
      
      if defined $filename and $filename.IO.e {
          $codeview.text = $filename.IO.slurp;
      } else  {
          $codeview.text = q:to/.../;
              sub frame($_, $t, $dt) {
                  .scale(2, 2);
                  .rgb(0, 0, 0);
                  .rectangle(0, 0, 150, 150);
                  .fill();
                  .rgb(1, 0.75, 0.1);
                  .move_to(sin($t) * 50 + 75, 50);
                  .line_to(0, cos($t) * 50) :relative;
                  .stroke();
              }
              ...
      }
      
      signal(SIGINT).tap({ $app.exit; });
      #signal(SIGTERM).tap({ $app.exit; });
    });
    
    TEPL::Main.init;
    $app.run();
    TEPL::Main.end;
    
    END {
        if $last_working_code_text {
            $filename = sprintf(
              "session-%4d%02d%02d-%02d%02d%02d.p6",
              .year, .month, .day, .hour, .minute, .second
            ) given DateTime.now;
            $filename.IO.spurt($last_working_code_text);
            say "You can find the last version of your code in $filename";
        }
    }
}

use GTK::Simple;
use GTK::Simple::App;

sub MAIN($filename? is copy) {
  my GTK::Simple::App $app .= new: title => "Cairo Live-Coding environment";

  $app.border-width = 10;

  $app.set_content(
      GTK::Simple::VBox.new(
          GTK::Simple::HBox.new(
              GTK::Simple::VBox.new(
                  my $codeview = GTK::Simple::TextView.new(),
                  my $fps_toggle = GTK::Simple::ToggleButton.new(label => "slow mode"),
              ),
              my $da       = GTK::Simple::DrawingArea.new()
          ),
          my $statuslabel = GTK::Simple::Label.new(text => "ready when you are.")
      ));

  $app.size_request(800, 600);
  $codeview.size_request(400, 550);
  $da.size_request(400, 550);
  
  signal(SIGINT).tap(  { exit(); });
  $app.run();
}
